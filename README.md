# Market Report BDD Protractor Framework

## Running SmokeTest

Run `node ./node_modules/protractor/bin/protractor ./e2e/protractor.conf.js --cucumberOpts.tags='@Smoke' --envVal='qa' --userName='username' --pwd='password'` to execute Smoke Suite.

## Running Regression

Run `node ./node_modules/protractor/bin/protractor ./e2e/protractor.conf.js --cucumberOpts.tags='@Regression' --envVal='qa' --userName='username' --pwd='password'` to execute Regression Suite.

## Running Acceptance Criteria Associated with JIRA Story

Run `node ./node_modules/protractor/bin/protractor ./e2e/protractor.conf.js --cucumberOpts.tags='@RM-3819' --envVal='qa' --userName='username' --pwd='password'` to execute Acceptance Criteria Associated with JIRA Story.

## Cucumber HTML report will get generated inside the project root folder:

`e2e\cucumber_report`
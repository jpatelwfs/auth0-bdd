#!/bin/bash
echo $1 $2
docker run --rm --name e2e --net=host -v $PWD:/app saketjain/node-chromium-protractor /bin/bash -c "npm config set registry http://jfrog.aws.wfscorp.com/artifactory/api/npm/npm; npm install; node /usr/local/lib/node_modules/protractor/bin/protractor /app/e2e/protractor.conf.js --chromeDriver=/usr/bin/chromedriver --cucumberOpts.tags=$2 --envVal=$1"
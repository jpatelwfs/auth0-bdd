//protractor.conf.js
const {
    SpecReporter
  } = require('jasmine-spec-reporter');
  const reporter = require('cucumber-html-reporter');
  exports.config = {
    restartBrowserBetweenTests: true,
    //SELENIUM_PROMISE_MANAGER: false,
    useAllAngular2AppRoots: true,
    //seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    //seleniumAddress: 'http://localhost:4444/wd/hub/',
    getPageTimeout: 60000,
    allScriptsTimeout: 500000,
    coloredLogs: true,
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    directConnect: true,
    baseURL: 'https://bmt-ui.test.aws.wfscorp.com/transaction',
    framework: 'custom',
    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    // Spec patterns are relative to this directory.
    specs: [
      'features/**/*.feature'
    ],

    capabilities: {
      'browserName': 'chrome',
      'time-zone': 'New York',
      'unexpectedAlertBehaviour': 'accept',
       chromeOptions: {
         args: [
          '--disable-infobars',
          '--window-size=1920,1080',
          '--headless', 
          '--no-sandbox',
          '--disable-gpu',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage',
        ]
       }
    },
      
    cucumberOpts: {
      require: [
        'features/**/*.step.ts',
        'afterHooks.js'
      ],
      failures: 'features/**/*.step.ts',
      // require: 'features/_sample/*.step.ts',
      tags: false,
      //format: 'pretty',
      failFast: false,
      profile: false,
      'no-source': true,
      ignoreUncaughtExceptions: true,
      format: [
        'json:' + require('path').join(__dirname, './results.json'),
        'progress',
      ],
      timeout: 20000,
    },

    onPrepare() {
      require('ts-node').register({
        project: require('path').join(__dirname, './tsconfig.e2e.json')
      });
    },
    
    onComplete: () => {
      reporter.generate({
        //'bootstrap', 'hierarchy', 'foundation', 'simple'
        theme: 'bootstrap',
        ignoreBadJsonFile: true,
        jsonFile: require('path').join(__dirname, './results.json'),
        output: require('path').join(__dirname, './cucumber_report.html'),
        screenshotsDirectory: './screenshots/',
        storeScreenshots: true,
        reportSuiteAsScenarios: true,
        // launchReport: true,
      });
    },
  };



//   //protractor.conf.js
// exports.config = {
//   restartBrowserBetweenTests: true,
//   seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
//   getPageTimeout: 60000,
//   allScriptsTimeout: 500000,
//   framework: 'custom',
//   // path relative to the current config file
//   frameworkPath: require.resolve('protractor-cucumber-framework'),
//   capabilities: {
//     'browserName': 'chrome'
//   },

//   // Spec patterns are relative to this directory.
//   specs: [
//     'features/**/*.feature'
//   ],

//   baseURL: 'https://bmt-ui.test.aws.wfscorp.com/transaction',

//   cucumberOpts: {
//     //require: 'features/step_definitions/stepDefinitions.js',
//     require: 'features/**/*.step.ts',
//     tags: false,
//     //format: 'pretty',
//     profile: false,
//     'no-source': true
//   },
//   onPrepare() {
//     require('ts-node').register({
//       project: require('path').join(__dirname, './tsconfig.e2e.json')
//     });
//   }
// };
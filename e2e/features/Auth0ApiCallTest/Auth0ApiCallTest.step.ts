import { accessTokenService } from '../../services/AccessTokenService';
import { listOrSearchUsersService } from '../../services/ListOrSearchUsersService';

const { Given, When, Then } = require('cucumber');
const expect = require('chai').expect;

Given('a user has access token to invoke auth0 APIs', { timeout: 60 * 1000 }, async () => {

    await accessTokenService.getAccessToken();
});

When('user invoke List Or Search Users Service of authO', { timeout: 60 * 1000 }, async () => {

    await listOrSearchUsersService.getListOrSearchUsersService();
});

Then('service rersponse body gets stored in a JSON file', { timeout: 60 * 1000 }, async () => {

    const fs = require('fs')
    const path = process.cwd() + '/e2e/resources/userprofile.json';
    let flag = false;
    try {
        if (fs.existsSync(path)) {
            flag = true;
        }
    } catch (err) {
        console.error(err)
    }

    expect(flag).to.be.equal(true);
});
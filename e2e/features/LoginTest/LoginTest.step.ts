import { securityUtil } from '../../services/SecurityUtils';
import { browser, by, element, WebElementPromise } from "protractor";

const { Given, When, Then } = require('cucumber');
const expect = require('chai').expect;

let loginData = require('../../config/login.json');
let jsonData = require('../../resources/userprofile.json');

let env;
let ld;
var name;
var user_name;
var preferred_username;
var given_name;
var family_name;
var email;
var customerIds;
var roles;

for (let j = 0; j < process.argv.length; j++) {
    if (process.argv[j].includes('--envVal')) {
        env = process.argv[j].split('=')[1];
    }
}

if (loginData) {
    if (env === 'dev') {
        loginData = loginData.environment[env];
    } else if (env === 'test') {
        loginData = loginData.environment[env];
    } else if (env === 'qa') {
        loginData = loginData.environment[env];
    } 
}

async function waitForPageToload() {

    for (var i = 0; i < 10000; i++) {

        var result = await browser.executeScript('return document.readyState');
        try {
            await browser.sleep(100);
        } catch (err) {
            console.log("System error -> " + err);
        }

        if (result == 'complete') {
            break;
        }
    }
}

Given('a user navigates to Auth0 SSO page via myWorld Marine Portal URL', { timeout: 60 * 1000 }, async () => {

    //let loginData = require('../../config/login.json');
    console.log('[env] => ' + env);
    console.log('loginData.environment[env].baseUrl => ' + loginData.baseUrl);
    await browser.driver.get(loginData.baseUrl);
    await browser.manage().window().maximize();
    waitForPageToload();
    await browser.sleep(5000);
});

When('user types username {string} and password', { timeout: 60 * 1000 }, async (userType) => {

    //loginData = loginData.environment[env];

    switch (userType) {
        case 'customerSA':
            ld = loginData.singleAccount;
            break;
        case 'customerMA':
            ld = loginData.multiAccount;
            break;
        case 'customerNA':
            ld = loginData.noAccount;
            break;
        case 'customerNoTermsAndConditions':
            ld = loginData.noTermsAndConditions;
            break;
        case 'marineTech':
            ld = loginData.marineTech;
            break;
        case 'marineTech2':
            ld = loginData.marineTech2;
            break;
        case 'customerMA2':
            ld = loginData.multiAccount2;
            break;
        case 'noAccount':
            ld = loginData.noAccount;
            break;
        case 'marineTech_mps_marine_tech':
            ld = loginData.marineTech_mps_marine_tech;
            break;
        case 'mp_inqof_readqa':
            ld = loginData.mp_inqof_readqa;
            break;
        case 'marinePortalUser_mps_wfs_read_only':
            ld = loginData.marinePortalUser_mps_wfs_read_only;
            break;
        case 'marinePortalUser_mps_wfs_admin':
            ld = loginData.marinePortalUser_mps_wfs_admin;
            break;
        case 'marinePortalUser_mps_customer_admin':
            ld = loginData.marinePortalUser_mps_customer_admin;
            break;
        case 'marinePortalUser_mp_acct':
            ld = loginData.marinePortalUser_mp_acct;
            break;
        case 'marinePortalUser_mp_customer_full':
            ld = loginData.marinePortalUser_mp_customer_full;
            break;
        case 'mp_inq_read_createqa':
            ld = loginData.mp_inq_read_createqa;
            break;
        case 'customerPrefSaved':
            ld = loginData.customerPrefSaved;
            break;
        case 'customerPrefSavedDev':
            ld = loginData.customerPrefSavedDev;
            break;
        case 'mpStdCustomerAcct':
            ld = loginData.mpStdCustomerAcct;
            break;
        case 'mpCustomerAcct':
            ld = loginData.mpCustomerAcct;
            break;
        case 'myWorldMarineStemOfferReadOnly':
            ld = loginData.myWorldMarineStemOfferReadOnly;
            break;
        case 'myWorldMarineStemOfferSelection':
            ld = loginData.myWorldMarineStemOfferSelection;
    }

    await browser.waitForAngularEnabled(false);
    await browser.driver.findElement(by.css('[name="username"]')).sendKeys(ld.username);
    await browser.driver.findElement(by.css('[name="password"]')).sendKeys(ld.password);
});

When('user type invalid username or password', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(false);
    let loginData = require('../../config/login.json');
    var randomNumber = Math.floor(Math.random()*1000)
    await browser.driver.findElement(by.css('[name="username"]')).sendKeys(loginData.invalidLoginCredentials.username + '_' + randomNumber);
    await browser.driver.findElement(by.css('[name="password"]')).sendKeys(loginData.invalidLoginCredentials.password);
});

When('user type blocked username or password', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(false);
    let loginData = require('../../config/login.json');
    await browser.driver.findElement(by.css('[name="username"]')).sendKeys(loginData.blockedLoginCredentials.username);
    await browser.driver.findElement(by.css('[name="password"]')).sendKeys(loginData.blockedLoginCredentials.password);
});

Then('user should sees global message saying {string}', { timeout: 60 * 1000 }, async (message) => {

    await browser.waitForAngularEnabled(true);
    const strMeg = await browser.driver.findElement(by.css('[class="auth0-global-message auth0-global-message-error"]')).getText();
    expect(strMeg).to.contains(message);
});

When('click on Sign in button', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(false);
    await browser.driver.findElement(by.css('[aria-label="Sign In"]')).click();
    await securityUtil.fetchToken();
    await browser.sleep(30000);
});

Then('myWorld Marine Portal Dashoard is displayed', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(true);
    const userFirstName = await browser.driver.findElement(by.css('wfc-dashboard .layout-header h1')).getText();
    expect(userFirstName.toUpperCase()).to.contains(("Welcome " + ld.firstName).toUpperCase());
});

When('user roles and customer are assigned and mapped correctly with auth0 user profile', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(false);

    var userToken = await securityUtil.fetchToken();
    var decodedString = Buffer.from(String(userToken), 'base64').toString();

    name = "(?<=name\":\")(?!$).*(?=\",\"user_name)";
    name = decodedString.match(name);

    user_name = "(?<=user_name\":\")(?!$).*(?=\",\"preferred_username)";
    user_name = decodedString.match(user_name);

    preferred_username = "(?<=preferred_username\":\")(?!$).*(?=\",\"username)";
    preferred_username = decodedString.match(preferred_username);

    given_name = "(?<=given_name\":\")(?!$).*(?=\",\"family_name)";
    given_name = decodedString.match(given_name);

    family_name = "(?<=family_name\":\")(?!$).*(?=\",\"email)";
    family_name = decodedString.match(family_name);

    email = "(?<=email\":\")(?!$).*(?=\",\"CustomerIds)";
    email = decodedString.match(email);

    customerIds = "(?<=CustomerIds\":[)([?!$]).*(?=],\"CustomerSiteIds)";
    customerIds = decodedString.match(customerIds);

    roles = "(?<=roles\":)(?!$).*(?=,\"iss)";
    roles = decodedString.match(roles);

    for (var i = 0; i <= jsonData.length - 1; i++) {

        if (jsonData[i].username == user_name && jsonData[i].app_metadata.customerIds !== undefined) {

            console.log('name => ' + name);
            console.log('jsonData[i].name => ' + jsonData[i].name);

            expect(name).to.contain(jsonData[i].name);

            expect(user_name).to.contain(jsonData[i].username);

            expect(preferred_username).to.contain(jsonData[i].username);

            expect(given_name).to.contain(jsonData[i].given_name);

            expect(family_name).to.contain(jsonData[i].family_name);

            expect(email).to.contain(jsonData[i].email);

            if (jsonData[i].username !== 'marinetechqa' || jsonData[i].username !== 'marinetechqa2' || jsonData[i].username !== 'portalmarinenoacct' || jsonData[i].username !== 'mpcustomeracct') {

                // Company ID
                let tokenCompanyID = [];
                tokenCompanyID = String(customerIds).split(',');
                let jsonDataCompanyID = [];
                jsonDataCompanyID = String(jsonData[i].app_metadata.customerIds).split(',');
                let count = 0;

                expect(tokenCompanyID.length).to.be.equal(jsonDataCompanyID.length);

                for (var j = 0; j <= tokenCompanyID.length - 1; j++) {

                    for (var k = 0; k <= tokenCompanyID.length - 1; k++) {

                        if (tokenCompanyID[j].includes(jsonDataCompanyID[k])) {
                            count = count + 1;
                            console.log('count => ' + count);
                            break;
                        }
                    }
                }
                expect(tokenCompanyID.length).to.be.equal(count);
            }

            if(ld.userRoles !== null){

                if (jsonData[i].username !== 'mp_inq_read_createqa' || jsonData[i].username !== 'mp_inqof_readqa' || jsonData[i].username !== 'portalmarinenoacct') {
                
                    console.log('rolesToAssign => ' + jsonData[i].app_metadata.rolesToAssign);
    
                    // User Roles
                    let tokenRoles = [];
                    tokenRoles = String(roles).split(',');
                    let loginDataRolesArray = [];
                    loginDataRolesArray = ld.userRoles;
                    let count = 0;
    
                    expect(tokenRoles.length).to.be.equal(loginDataRolesArray.length);
    
                    for (var i = 0; i <= tokenRoles.length - 1; i++) {
    
                        for (var j = 0; j <= tokenRoles.length - 1; j++) {
    
                            if (tokenRoles[i].includes(loginDataRolesArray[j])) {
                                count = count + 1;
                                console.log('count => ' + count);
                                break;
                            }
                        }
                    }
                    expect(tokenRoles.length).to.be.equal(count);
                }
            }            
            break;
        }
    }
    await browser.sleep(5000);
});

When('the user navigates to the Settings page', { timeout: 60 * 1000 }, async () => {

    let el = await browser.findElement(by.css(`.menu-item__user`));
    await browser.executeScript('arguments[0].click()', el);
    await browser.sleep(1500);
    await browser.findElement(by.css('[ng-reflect-value="account-preferences"]')).click();
    await browser.sleep(3000);
});

Then('the user is navigated to the User Preferences page', { timeout: 60 * 1000 }, async () => {

    await browser.sleep(3000);
    expect(await browser.getCurrentUrl()).to.contain(`account-preferences`);
});

Then('the Header information is correct for the user First and Last Name, Email, and Company - User Profile Settings', { timeout: 60 * 1000 }, async () => {

    // First Name
    let firstName = await browser.findElement(by.css('[ng-reflect-label="First Name"] .editable-component-value')).getText();
    expect(firstName).to.contain(ld.firstName);
    expect(firstName).to.contain(given_name);

    // Last Name
    let lastName = await browser.findElement(by.css('[ng-reflect-label="Last Name"] .editable-component-value')).getText();
    expect(lastName).to.contain(ld.lastName);
    expect(lastName).to.contain(family_name);

    // Email
    let uiEmail = await browser.findElement(by.css('[ng-reflect-label="Email"] .editable-component-value')).getText();
    expect(uiEmail).to.contain(ld.email);
    expect(uiEmail).to.contain(email);

    // Company Name
    let companyStr = await browser.findElement(by.css('[ng-reflect-label="Account Name"] .editable-component-value')).getText();
    let companyArray = [];
    companyArray = companyStr.split(',');
    let loginDataCompanyArray = [];
    loginDataCompanyArray = ld.company;
    let count = 0;

    expect(companyArray.length).to.be.equal(loginDataCompanyArray.length);

    for (var i = 0; i <= companyArray.length - 1; i++) {

        for (var j = 0; j <= companyArray.length - 1; j++) {

            if (companyArray[i].includes(loginDataCompanyArray[j])) {
                count = count + 1;
                console.log('count => ' + count);
                break;
            }
        }

    }
    expect(companyArray.length).to.be.equal(count);

    // Company ID
    let tokenCompanyID = [];
    tokenCompanyID = String(customerIds).split(',');
    let loginDataCompanyIDArray = [];
    loginDataCompanyIDArray = ld.companyID;
    count = 0;

    expect(tokenCompanyID.length).to.be.equal(loginDataCompanyIDArray.length);

    for (var i = 0; i <= tokenCompanyID.length - 1; i++) {

        for (var j = 0; j <= tokenCompanyID.length - 1; j++) {

            if (tokenCompanyID[i].includes(loginDataCompanyIDArray[j])) {
                count = count + 1;
                console.log('count => ' + count);
                break;
            }
        }

    }
    expect(tokenCompanyID.length).to.be.equal(count);
});

Then('marine tech myWorld Marine Portal Dashoard is displayed', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(true);
    const welcomeMessage = await browser.driver.findElement(by.css('[class="layout-header-title"]')).getText();
    expect(welcomeMessage).to.contains('Marine Tech Requests');
});

Then('the error message is displayed {string}', { timeout: 60 * 1000 }, async (message) => {

    await browser.sleep(3000);
    let errorMessage = await browser.findElement(by.css('[class="title"]')).getText();
    expect(errorMessage).to.contain(message);
});

When('user click on Reset Password', { timeout: 60 * 1000 }, async () => {

    await browser.waitForAngularEnabled(false);
    await browser.driver.findElement(by.xpath('//a[text()="Reset Password"]')).click();
});

When('type email address {string}', { timeout: 60 * 1000 }, async (emailAddress) => {

    await browser.sleep(3000);
    await browser.driver.findElement(by.css('[name="email"]')).sendKeys(emailAddress);
});

Then('send email button is disabled', { timeout: 60 * 1000 }, async () => {

    let sampleElement: WebElementPromise = element(by.css('[aria-label="Send email"]')).getWebElement();
    expect(sampleElement.getAttribute('disabled')).exist;
    await browser.sleep(3000);
});

Then('click on send email button', { timeout: 60 * 1000 }, async () => {

    await browser.driver.findElement(by.css('[aria-label="Send email"]')).click();
    await browser.sleep(3000);
});

Then('user should sees success message saying {string}', { timeout: 60 * 1000 }, async (successMessage) => {

    await browser.sleep(2000);
    // const strMeg = await browser.driver.findElement(by.css('[class="auth0-global-message auth0-global-message-success"] span span br')).getText();
    // expect(strMeg).to.contains(successMessage);
    let sampleElement: WebElementPromise = element(by.css('[class="auth0-global-message auth0-global-message-success"]')).getWebElement();
    expect((await sampleElement).isDisplayed());
});
Feature: Auth0 login test for Existing - External User has an Auth0 app and KC app

    @Regression @Smoke @auth0Login @invalidLogin @myWorldMarinePortal
    Scenario Outline: Invalid login credientials - Auth0 login for client myWorld marine Portal
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user type invalid username or password
        And click on Sign in button
        Then user should sees global message saying "<message>"

        Examples:
            | message                                                                                                                                                                                        |
            | Sorry we are not able to identify your information in our system. Please try again, or if you have recently changed your username or email address, please call 1-888-939- 4852 for assistance |

    @Regression @Smoke @auth0Login @blockedAccount @myWorldMarinePortal
    Scenario Outline: Blocked User - Auth0 login for client myWorld marine Portal
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user type blocked username or password
        And click on Sign in button
        Then user should sees global message saying "<message>"

        Examples:
            | message                                                                  |
            | Your account has been blocked after multiple consecutive login attempts. |

    @Regression @Smoke @auth0Login @restPasswordInternal @myWorldMarinePortal
    Scenario Outline: Rest password - Auth0 Global Message for Internal Users
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user click on Reset Password
        And type email address "<emailAddress>"
        Then send email button is disabled
        And user should sees global message saying "<message>"

        Examples:
            | emailAddress          | message                                                                                                                         |
            | auth0test@wfscorp.com | Your email's domain is part of an Enterprise identity provider. To reset your password, please see your security administrator. |

    @Regression @Smoke @auth0Login @restPasswordExternal @myWorldMarinePortal
    Scenario Outline: Rest password - Auth0 Global Message for External Users
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user click on Reset Password
        And type email address "<emailAddress>"
        And click on send email button
        Then user should sees success message saying "<message>"

        Examples:
            | emailAddress        | message                                              |
            | auth0test@gmail.com | We've just sent you an email to reset your password. |

    @Regression @Smoke @myWorldMarinePortalAssignedUserRolesAndCustomers @myWorldMarinePortal
    Scenario Outline: Auth0 login for client myWorld marine Portal with User Roles and Customers are assigned and Update Password OFF on KC
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user types username "<userType>" and password
        And click on Sign in button
        Then myWorld Marine Portal Dashoard is displayed
        And user roles and customer are assigned and mapped correctly with auth0 user profile
        And the user navigates to the Settings page
        Then the user is navigated to the User Preferences page
        And the Header information is correct for the user First and Last Name, Email, and Company - User Profile Settings

        Examples:
            | userType                            |
            | customerMA                          |
            | customerSA                          |
            | mpStdCustomerAcct                   |
            | customerPrefSaved                   |
            | customerMA2                         |
            | marinePortalUser_mps_wfs_read_only  |
            | marinePortalUser_mps_wfs_admin      |
            | marinePortalUser_mp_customer_full   |
            | marinePortalUser_mps_customer_admin |
            | marinePortalUser_mp_acct            |
            | customerNoTermsAndConditions        |
            | myWorldMarineStemOfferReadOnly      |
            | myWorldMarineStemOfferSelection     |

    @Regression @Smoke @myWorldMarinePortalMarintTechUsers @myWorldMarinePortal
    Scenario Outline: Marine Tech Users - Auth0 login for client myWorld marine Portal with User Roles and Customers are assigned and Update Password OFF on KC
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user types username "<userType>" and password
        And click on Sign in button
        Then marine tech myWorld Marine Portal Dashoard is displayed
        And user roles and customer are assigned and mapped correctly with auth0 user profile

        Examples:
            | userType                   |
            | marineTech_mps_marine_tech |
            | marineTech                 |
            | marineTech2                |

    @Regression @Smoke @myWorldMarinePortalUnassignedUserRoles @myWorldMarinePortal
    Scenario Outline: Auth0 login for client myWorld marine Portal with Unassigned User Roles but Assigned Customers and Update Password OFF on KC
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user types username "<userType>" and password
        And click on Sign in button
        Then the error message is displayed "<message>"
        And user roles and customer are assigned and mapped correctly with auth0 user profile

        Examples:
            | userType             | message                                           |
            | mp_inq_read_createqa | We're sorry. You do not have access to this page. |
            | mp_inqof_readqa      | We're sorry. You do not have access to this page. |

    @Regression @Smoke @myWorldMarinePortalUnassignedCustomers @myWorldMarinePortal
    Scenario Outline: Auth0 login for client myWorld marine Portal with Assigned User Roles but Unassigned Customers and Update Password OFF on KC
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user types username "<userType>" and password
        And click on Sign in button
        Then the error message is displayed "<message>"
        And user roles and customer are assigned and mapped correctly with auth0 user profile

        Examples:
            | userType       | message                            |
            | mpCustomerAcct | No customer defined for this user. |

    @Regression @Smoke @myWorldMarinePortalUnassignedUserRolesAndCustomers @myWorldMarinePortal
    Scenario Outline: Auth0 login for client myWorld marine Portal with Unassigned User Roles and Customers and Update Password OFF on KC
        Given a user navigates to Auth0 SSO page via myWorld Marine Portal URL
        When user types username "<userType>" and password
        And click on Sign in button
        Then the error message is displayed "<message>"
        And user roles and customer are assigned and mapped correctly with auth0 user profile

        Examples:
            | userType  | message                                           |
            | noAccount | We're sorry. You do not have access to this page. |
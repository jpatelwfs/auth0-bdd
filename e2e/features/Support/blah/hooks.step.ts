import { Before, After, Status } from 'cucumber';
import { browser } from 'protractor';

After(async function (scenario) {
  if (scenario.result.status === Status.FAILED) {
    const screenShotFail = await browser.takeScreenshot();
    this.attach(screenShotFail, 'image/png');
  }
});

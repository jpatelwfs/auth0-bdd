import { json } from '../features/Support/blah/json';
import * as request from 'request';
import { accessTokenService } from './AccessTokenService';

class SearchUsersByEmailService {

    private url: String;

    public getsearchUsersByEmailService(userEmail) {

        this.url = 'https://qa-wfs.auth0.com/api/v2/users-by-email?email=' + userEmail;

        const requestObj = {
            url: this.url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessTokenService.accessToken}`
            },
        };

        return new Promise((resolve, reject) => {
            request.get(requestObj, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(body));
                    json.writeJSON(body);
                }
            });
        });
    }
}
export const searchUsersByEmailService = new SearchUsersByEmailService();
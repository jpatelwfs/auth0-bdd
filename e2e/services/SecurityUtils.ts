import { browser } from 'protractor';

class SecurityUtil {

    public token: any;
    public customerIDs: any;

    public fetchToken() {
        return new Promise(async (resolve, reject) => {
            this.token = await browser.executeScript('return window.sessionStorage.getItem(\'token\')');
            this.customerIDs = await browser.executeScript('return window.sessionStorage.getItem(\'customerIds\')');
            resolve(this.token);
        });        
    }
}
export const securityUtil = new SecurityUtil();
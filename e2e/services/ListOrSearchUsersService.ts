import { json } from '../features/Support/blah/json';
import * as request from 'request';
import { accessTokenService } from './AccessTokenService';

class ListOrSearchUsersService {

    private url: String;

    public getListOrSearchUsersService() {

        this.url = 'https://qa-wfs.auth0.com/api/v2/users';

        const requestObj = {
            url: this.url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessTokenService.accessToken}`
            },
            body: '{"q":"identities.connection:"Shared-WFS-External"}' 
        };

        return new Promise((resolve, reject) => {
            request.get(requestObj, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(body));
                    json.writeJSON(body);
                }
            });
        });
    }
}
export const listOrSearchUsersService = new ListOrSearchUsersService();
import * as request from 'request';

class AccessTokenService {

    private url: String;
    public accessToken: any;

    public getAccessToken() {

        this.url = 'https://qa-wfs.auth0.com/oauth/token';
        
        const requestObj = {
            url: this.url,
            headers: {
                'Content-Type': 'application/json',
            },
            body: '{"client_id":"5VdJpei2e5Iemrk0e8dVFjsgEe0RIgtj",' + 
                   '"client_secret":"w8O4C8Uw27jutBILs-dW7pLtTB5xj2BpShxqsJ6M-w0akKAk_tyxC9936P25j4wI",' +
                   '"audience":"https://qa-wfs.auth0.com/api/v2/",' +
                   '"grant_type":"client_credentials"}'
        };
        
        return new Promise((resolve, reject) => {
            request.post(requestObj, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(body));
                    this.accessToken = body.match('(?<=access_token\":\")(?!$).*(?=\",\"scope)');
                }
            });
        });        
    }
}
export const accessTokenService = new AccessTokenService();